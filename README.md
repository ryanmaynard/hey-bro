# [Hey Bro][site]

# <img src="assets/heybro.png" alt="Hey Bro text blobl" width="150"/>

[![Netlify Status](https://api.netlify.com/api/v1/badges/951c3f37-6b1e-4482-b326-c44c1dc0fe13/deploy-status)](https://app.netlify.com/sites/getrekticu/deploys)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# <img src="demo.png" alt="demo screenshot" width="600"/>

#### Description

A list of ways to summon your bros


#### Contributing

Be sure to review the [Contributing Guide][contributing] before submitting a Merge Request.

1. Fork it `https://gitlab.com/ryanmaynard/hey-bro/forks/new`
2. Create your feature branch `git checkout -b my-new-feature`
3. Commit your changes `git commit -am 'Add some feature'`
4. Push to the branch `git push origin my-new-feature`
5. Create a new Merge Request `https://gitlab.com/ryanmaynard/hey-bro/merge_requests/new`



#### License

[MIT TLDR][tldr]
[License Text][license]

[site]: https://heybro.casa
[contributing]: CONTRIBUTING.md
[tldr]: https://tldrlegal.com/license/mit-license
[license]: https://gitlab.com/ryanmaynard/hey-bro/blob/master/LICENSE